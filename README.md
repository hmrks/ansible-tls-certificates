# ansible-tls-certificates

This is a collection of playbooks showcasing Ansible's ability to easily generate self-signed certificates and full certificate chains. It is for educational purposes only.

##### Dependencies

The following playbooks use the [community.crypto](https://docs.ansible.com/ansible/latest/collections/community/crypto/index.html) collection. To install, use `ansible-galaxy collection install community.crypto`. For additional dependencies, see the individual Ansible modules' description:

* [community.crypto.openssl_privatekey](https://docs.ansible.com/ansible/latest/collections/community/crypto/openssl_privatekey_module.html#ansible-collections-community-crypto-openssl-privatekey-module)
* [community.crypto.openssl_csr](https://docs.ansible.com/ansible/latest/collections/community/crypto/openssl_csr_module.html#ansible-collections-community-crypto-openssl-csr-module)
* [community.crypto.x509_certificate](https://docs.ansible.com/ansible/latest/collections/community/crypto/x509_certificate_module.html#ansible-collections-community-crypto-x509-certificate-module)

### self_signed.yaml

Example playbook for creating a self-signed certificate. It can be executed with:

```
ansible-playbook self_signed.yaml
```

By default, it will use the common name _www.example.com_, and will name all related files (the key, the certificate signing request and the certificate) _selfsigned.*_.

### ca_plus_client.yaml

Example playbook for creating a Certificate Authority and signing a client certificate with it. It can be executed with:

```
ansible-playbook ca_plus_client.yaml
```

All defaults can (should) be overwritten.

### ca_full_chain.yaml

Example playbook for creating a Certificate Authority, an intermediate CA and a client certificate. It can be executed with:

```
ansible-playbook ca_full_chain.yaml
```

Dummy defaults, as with previous playbooks.
