---
- name: Create CA, intermediate CA and client certificate.
  hosts: localhost
  connection: local
  become: false
  vars:
    ca: {var: 'ca', cn: 'my-ca', country: 'HU', email: 'ca@example.com', locality: 'Budapest', org: 'My Org'}
    intermediate: {var: 'int', cn: 'my-int-ca', country: 'HU', email: 'int@example.com', locality: 'Budapest', org: 'My Org'}
    client: {var: 'client', cn: 'client', country: 'HU', email: 'client@example.com', locality: 'Budapest', org: 'My Client', san: ['DNS:client.example.com', 'DNS:*.client.example.com']}
  tasks:
    - name: Generate key pair for CA.
      community.crypto.openssl_privatekey:
        path: "{{ playbook_dir }}/{{ ca.var }}.pem"
        size: 2048
    - name: Generate CSR for CA.
      community.crypto.openssl_csr:
        path: "{{ playbook_dir }}/{{ ca.var }}.csr"
        privatekey_path: "{{ playbook_dir }}/{{ ca.var }}.pem"
        common_name: "{{ ca.cn }}"
        country_name: "{{ ca.country }}"
        locality_name: "{{ ca.locality }}"
        email_address: "{{ ca.email }}"
        organization_name: "{{ ca.org }}"
        key_usage:
          - digitalSignature
          - keyCertSign
          - cRLSign
        key_usage_critical: true
        basic_constraints:
          - 'CA:TRUE'
        basic_constraints_critical: true
        use_common_name_for_san: false
    - name: Generate CA certificate.
      community.crypto.x509_certificate:
        path: "{{ playbook_dir }}/{{ ca.var }}.crt"
        privatekey_path: "{{ playbook_dir }}/{{ ca.var }}.pem"
        csr_path: "{{ playbook_dir }}/{{ ca.var }}.csr"
        provider: selfsigned
    - name: Generate key pair for intermediate CA.
      community.crypto.openssl_privatekey:
        path: "{{ playbook_dir }}/{{ intermediate.var }}.pem"
        size: 2048
    - name: Generate CSR for intermediate CA.
      community.crypto.openssl_csr:
        path: "{{ playbook_dir }}/{{ intermediate.var }}.csr"
        privatekey_path: "{{ playbook_dir }}/{{ intermediate.var }}.pem"
        common_name: "{{ intermediate.cn }}"
        country_name: "{{ intermediate.country }}"
        locality_name: "{{ intermediate.locality }}"
        email_address: "{{ intermediate.email }}"
        organization_name: "{{ intermediate.org }}"
        key_usage:
          - digitalSignature
          - keyCertSign
          - cRLSign
        key_usage_critical: true
        basic_constraints:
          - 'CA:TRUE'
          - 'pathlen:0'
        basic_constraints_critical: true
        use_common_name_for_san: false
    - name: Generate intermediate CA certificate.
      community.crypto.x509_certificate:
        path: "{{ playbook_dir }}/{{ intermediate.var }}.crt"
        csr_path: "{{ playbook_dir }}/{{ intermediate.var }}.csr"
        provider: ownca
        ownca_path: "{{ playbook_dir }}/{{ca.var}}.crt"
        ownca_privatekey_path: "{{ playbook_dir }}/{{ ca.var }}.pem"
    - name: Generate key pair for client.
      community.crypto.openssl_privatekey:
        path: "{{ playbook_dir }}/{{ client.var }}.pem"
        size: 2048
    - name: Generate CSR for client.
      community.crypto.openssl_csr:
        path: "{{ playbook_dir }}/{{ client.var }}.csr"
        privatekey_path: "{{ playbook_dir }}/{{client.var}}.pem"
        common_name: "{{ client.cn }}"
        country_name: "{{ client.country }}"
        locality_name: "{{ client.locality }}"
        email_address: "{{ client.email }}"
        organization_name: "{{ client.org }}"
        subject_alt_name: "{{ client.san }}"
    - name: Generate client certificate.
      community.crypto.x509_certificate:
        path: "{{ playbook_dir }}/{{ client.var }}.crt"
        csr_path: "{{ playbook_dir }}/{{ client.var }}.csr"
        provider: ownca
        ownca_path: "{{ playbook_dir }}/{{ intermediate.var }}.crt"
        ownca_privatekey_path: "{{ playbook_dir }}/{{ intermediate.var }}.pem"
